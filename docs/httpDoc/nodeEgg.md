# 个人通用后台接口文档
## 公用方法
### 请求量+1
::: tip /read
- Get
:::
### 登录
::: tip /login
- Post
- params:
    - name：String
    - password: String
:::

## 转发的第三方接口
### 获取热点新闻
::: tip /juheapi/news
- Get
- querys:
    - page
    - page_size
    - type
:::