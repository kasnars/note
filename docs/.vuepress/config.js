module.exports = {
    // theme: '@vuepress/blog',
    // themeConfig: {
    //     // 请参考文档来查看所有可用的选项。
    // },
    plugins: ['@vuepress/nprogress','@vuepress/back-to-top'],
    title: 'Kasnars\'s note',
    description: '个人接口文档及技术笔记',
    head: [ // 注入到当前页面的 HTML <head> 中的标签
        ['link', { rel: 'icon', href: 'logo.jpg' }], // 增加一个自定义的 favicon(网页标签的图标)
    ],
    palette: './styles/palette.styl',
    base: '/note/', // 这是部署到github相关的配置
    markdown: {
        lineNumbers: false // 代码块显示行号
    },
    themeConfig: {
        nav: [ // 导航栏配置
            {
                text: '个人笔记',
                items: [
                    {
                        text: '前端相关',
                        items: [
                            { text: 'HTML+CSS', link: '/frontend/htmlcss' },
                            { text: 'JavaScript', link: '/frontend/js' },
                            { text: 'TypeScript', link: '/frontend/ts' },
                            { text: 'Vue', link: '/frontend/vue' },
                            { text: 'React', link: '/frontend/react' }
                        ]
                    },
                    {
                        text: '后端相关',
                        items: [
                            { text: 'Egg.js', link: '/backend/egg' },
                            { text: 'Koa2', link: '/backend/koa2' },
                        ]
                    }
                ]
            },
            { text: '算法基础',
                items: [
                    { text: '算法', link: '/algorithm/algorithm' },
                    { text: '数据结构', link: '/algorithm/data' },
                    { text: '计算机网络', link: '/basics/network' },
                    { text: '浏览器原理', link: '/basics/browser' },
                ] 
            },
            {
                text: '接口文档',
                items: [
                    { text: 'Personal Node Backend', link: '/httpDoc/nodeEgg' },
                    { text: 'yuexia bbs', link: '/httpDoc/yuexiabbs' },
                    { text: '常用第三方接口整理', link: '/httpDoc/openapi' }
                ]
            },
            {
                text: '面经整理',
                link: '/viewnote/viewnote'
                // items: [
                //     { text: 'Personal Node Backend', link: '/httpDoc/nodeEgg' },
                // ]
            },
            {
                text: '其他项目',
                items: [
                    { text: '个人博客', link: 'http://kasnars.gitee.io/personalblog/#/' },
                    { text: '月下bbs', link: 'http://kasnars.club/' },
                ]
            },
            { text: '个人GIT', link: 'https://gitee.com/kasnars' }
        ],
        // sidebar: 'auto', // 侧边栏配置
        sidebarDepth: 2, // 侧边栏显示2级
        sidebar: [

            {
                title: '语言基础', // 侧边栏名称
                collapsable: true, // 可折叠
                children: [
                    '/frontend/js', // 你的md文件地址
                    '/frontend/ts'
                ]
            },
            {
                title: '前端框架',
                collapsable: true,
                children: [
                    '/frontend/vue',
                    '/frontend/react'
                ]
            },
            {
                title: '计算机基础',
                collapsable: true,
                children: [
                    '/algorithm/algorithm',
                    '/algorithm/data' ,
                    '/basics/browser',
                    '/basics/network'
                ]
            },
            {
                title: '面经大合集',
                collapsable: true,
                children: [
                    '/viewnote/viewnote'
                ]
            },
            // {
            //     title: '个人自用接口文档', // 侧边栏名称
            //     collapsable: true, // 可折叠
            //     children: [
            //         '/httpDoc/nodeEgg', // 你的md文件地址
            //     ]
            // },
        ]
    },

};