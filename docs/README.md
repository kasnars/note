---
home: true
heroImage: https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg1.doubanio.com%2Fview%2Fgroup_topic%2Fl%2Fpublic%2Fp509851599.jpg&refer=http%3A%2F%2Fimg1.doubanio.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1646836539&t=bd37d7a83b04b66a71eb416a39d03044
actionText: 点击开卷 →
actionLink: /frontend/js
heroText: Kasnars Note（项目迁移中）
tagline: 重大更新，请先查看底部TIPS
features:
- title: 个人笔记
  details: 记录面经和技术问题以及编程技巧等技术。



- title: 算法笔记
  details: 记录平时遇到的算法问题以及思路解法等。

- title: 接口文档
  details: 记录个人所写后台接口，大部分均设置好跨域，可直接调用。
footer: 个人静态资源部署站点，用作线上笔记学习和记录相关技术知识等，随缘更新  
---
::: tip
本站点大量使用vuepress自定义容器，IE/Edge可能出现兼容性问题
- [KasnarsNote Gitee ->](https://gitee.com/kasnars/note)  

项目结构升级，后续所有将同步更新至新的静态资源博客中
- [新的博客地址，短期内两边同步更新](http://kasnars.gitee.io/markdown-blog/)
:::